@ECHO OFF

ECHO Yanga Watchdog

REM ============================================================================================================================================================================

REM Als de debug flag op 1 of 2 staat wordt er extra info gelogt. Als de flag op 0 staat wordt er minimaal gelogt.
REM Standaard: SET /A "DebugFlag=0"

SET /A "DebugFlag=0"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Als de onderstaande waarde op 1 staat wordt er naar de heartbeat gekeken, indien de waarde op 0 staat wordt er alleen gekeken of de JNA app in het geheugen draait.
REM Standaard: SET /A "ScanHeartbeat=1"

SET /A "ScanHeartbeat=1"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoe lang er gewacht moet worden in seconden tussen scans om te kijken of de JNA app nog draait.
REM Standaard: SET /A "IdleTimeout=5"

SET /A "IdleTimeout=5"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoe lang er gewacht moet worden in seconden met een nieuwe scan nadat de JNA app wordt opgestart.
REM Standaard: SET /A "LaunchTimeout=10"

SET /A "LaunchTimeout=10"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele moet alijd op 0 staan, de beginwaarde van het aantal keer dat de JNA app is opgestart.
REM Standaard: SET /A "LaunchCount=0"

SET /A "LaunchCount=0"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoe vaak de JNA app mag worden opgestart voordat het hele systeem een reboot gegeven wordt.
REM Standaard: SET /A "LaunchLimit=3"

SET /A "LaunchLimit=3"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoeveel seconden er wordt gewacht met het opstarten van de app na het afsluiten van de app.
REM Standaard: SET /A "TerminateTimeout=5"

SET /A "TerminateTimeout=5"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoeveel seconden de heartbeat file oud mag zijn voordat de JNA app als vastgelopen wordt aangemerkt en de JNA app geforceerd opnieuw wordt opgestart.
REM Standaard: SET /A "HeartbeatTimeout=30"

SET /A "HeartbeatTimeout=30"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Als het later dan deze tijd is wordt de heartbeat gechecked.
REM Standaard: SET "HBLaterThan=000100"

SET "HBLaterThan=000100"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Als het vroeger dan deze tijd is wordt de heatbeat gechecked.
REM Standaard: SET "HBEarlierThan=235900"

SET "HBEarlierThan=235900"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt waar er wordt gezocht naar de heartbeat file. Zonder slash na de mapnaam!
REM Standaard: SET "HeartbeatPath=%USERPROFILE%\Videos"

SET "HeartbeatPath=%USERPROFILE%\Videos"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoe de heartbeat file heet waar naar moet worden gezocht. Hier inclusief extentie opgeven.
REM Standaard: SET "HeartbeatFile=Watchdog.txt"

SET "HeartbeatFile=Watchdog.txt"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele bepaalt hoe de logfile heet waar naartoe gelogt moet worden gezocht. File wordt in de script directory gezet. Geen extentie opgeven!
REM Standaard: SET "WatchdogLog=WatchdogLog"

SET "WatchdogLog=WatchdogLog"

REM ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

REM Deze variabele geeft aan waar dit script zich bevindt. Hoeft nooit aangepast te worden.
REM Standaard: SET ScriptPath=%~dp0
SET ScriptPath=%~dp0

REM ============================================================================================================================================================================





REM *** Crude log "rotation" ***
IF EXIST "%ScriptPath%%WatchdogLog%.old3.txt" MOVE /Y "%ScriptPath%%WatchdogLog%.old3.txt" "%ScriptPath%%WatchdogLog%.old4.txt">NUL
IF EXIST "%ScriptPath%%WatchdogLog%.old2.txt" MOVE /Y "%ScriptPath%%WatchdogLog%.old2.txt" "%ScriptPath%%WatchdogLog%.old3.txt">NUL
IF EXIST "%ScriptPath%%WatchdogLog%.old.txt" MOVE /Y "%ScriptPath%%WatchdogLog%.old.txt" "%ScriptPath%%WatchdogLog%.old2.txt">NUL
IF EXIST "%ScriptPath%%WatchdogLog%.txt" MOVE /Y "%ScriptPath%%WatchdogLog%.txt" "%ScriptPath%%WatchdogLog%.old.txt">NUL





REM *** ECHO config if debug flag is set ***

IF %DebugFlag% GTR 0 (
	ECHO =========== Watchdog config =========== >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - DebugFlag = %DebugFlag% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - ScanHeartbeat = %ScanHeartbeat% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - IdleTimeout = %IdleTimeout% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - LaunchTimeout = %LaunchTimeout% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - LaunchCount = %LaunchCount% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - LaunchLimit = %LaunchLimit% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - FocusDelay = %FocusDelay% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - TerminateTimeout = %TerminateTimeout% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - HeartbeatTimeout = %HeartbeatTimeout% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - HeartbeatPath = %HeartbeatPath% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - HeartbeatFile = %HeartbeatFile% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - HBLaterThan= %HBLaterThan% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - HBEarlierThan = %HBEarlierThan% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - ScriptPath = %ScriptPath% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO ======================================= >>"%ScriptPath%%WatchdogLog%.txt"
)





REM *** Check if app is present in the tasklist ***

:DetectApp
IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - Checking if app image is running... >>"%ScriptPath%%WatchdogLog%.txt"
tasklist /FI "IMAGENAME eq yanga_app.exe">"%ScriptPath%temp\TasklistOutput.txt"
FIND "No tasks are running" "%ScriptPath%temp\TasklistOutput.txt">NUL
IF %errorlevel% EQU 1 goto AppIsRunning
GOTO AppIsNotRunning





:AppIsRunning

REM *** If we are here, the app was detected in the tasklist and we are getting ready to check the heartbeat ***

IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - App is Running! >>"%ScriptPath%%WatchdogLog%.txt"

IF %ScanHeartbeat% EQU 0 GOTO SkipScan

REM *** Get neat & exception time ***

FOR /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') DO SET "dt=%%a"
SET "HH=%dt:~8,2%" & SET "Min=%dt:~10,2%" & SET "Sec=%dt:~12,2%"
SET NeatTime=%HH%:%Min%:%Sec%
SET ExceptionTime=%HH%%Min%%Sec%

REM *** Get time limit ***

SET hh=%TIME:~0,2%
SET mm=%TIME:~3,2%
SET ss=%TIME:~6,2%

set /a ss=10000%ss% %% 10000
set /a mm=10000%mm% %% 10000

SET /a sum_sec=%hh% * 3600 + %mm% * 60 + %ss% -%HeartbeatTimeout%
SET /a h=%sum_sec% / 3600
SET /a m=(%sum_sec%/60) - 60 * %h%
SET /a s=%sum_sec% - 60 * (%sum_sec%/60)
IF %h% LSS 10 SET h=0%h%
IF %m% LSS 10 SET m=0%m%
IF %s% LSS 10 SET s=0%s%
SET TimeLimit=%h%:%m%:%s%

REM *** Get file time ***

ECHO 0 >"%ScriptPath%temp\ForfilesOutput.txt"
forfiles /p "%HeartbeatPath%" /m "%HeartbeatFile%" /c "cmd /c ECHO @FTIME" >"%ScriptPath%temp\ForfilesOutput.txt"

FIND ":" "%ScriptPath%temp\ForfilesOutput.txt">NUL
IF %errorlevel% EQU 1 goto FileTimeNotRead

for /f "tokens=1*delims=:" %%G in ('findstr /n "^" "%ScriptPath%temp\ForfilesOutput.txt"') do if %%G equ 2 set FileStamp=%%H

REM *** ECHO heartbeat time info if debug flag is set ***

IF %DebugFlag% GTR 1 (
	ECHO ========= Heartbeat time info ========= >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - ExceptionTime = %ExceptionTime% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - NeatTime = %NeatTime% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - TimeLimit = %TimeLimit% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO - Filestamp = %FileStamp% >>"%ScriptPath%%WatchdogLog%.txt"
	ECHO ======================================= >>"%ScriptPath%%WatchdogLog%.txt"
)

REM *** Check if the time is outside the set limits around midnight ***

IF "%ExceptionTime%" GTR "%HBLaterThan%" (
	IF %DebugFlag% GTR 1 ECHO %DATE% %TIME% - It is later than 00:01:00. >>"%ScriptPath%%WatchdogLog%.txt"
	IF "%ExceptionTime%" LSS "%HBEarlierThan%" (

		REM *** Far enough from midnight, lets check the heartbeat ***

		IF %DebugFlag% GTR 1 ECHO %DATE% %TIME% - It is also earlier than 23:59:00, Ready to check heartbeat! >>"%ScriptPath%%WatchdogLog%.txt"
		IF "%FileStamp%" GTR "%TimeLimit%" (
			GOTO HeartbeatVerified
		) ELSE (
			GOTO AppHang
		)
	)
)

REM *** If we are here, we are in the limits around midnight ***

ECHO %DATE% %TIME% - Skipping heartbeat detection so close to midnight. >>"%ScriptPath%%WatchdogLog%.txt"
IF %DebugFlag% GTR 1 ECHO %DATE% %TIME% - Waiting %IdleTimeout% secs to scan again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %IdleTimeout% /NOBREAK>NUL
GOTO DetectApp





:HeartbeatVerified

REM *** If we are here, the heartbeat was just verified ***

ECHO %DATE% %TIME% - Heartbeat verified (file time is %FileStamp%, which is newer than %TimeLimit%) >>"%ScriptPath%%WatchdogLog%.txt"
IF %DebugFlag% GTR 1 ECHO %DATE% %TIME% - Waiting %IdleTimeout% secs to scan again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %IdleTimeout% /NOBREAK>NUL
GOTO DetectApp





:AppIsNotRunning

REM *** If we are here, the app was not found in the tasklist ***

IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - App is NOT Running! >>"%ScriptPath%%WatchdogLog%.txt"
IF %LaunchCount% GEQ %LaunchLimit% GOTO RebootNow
IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - Launch limit not reached (%LaunchCount%/%LaunchLimit%), Launching app... >>"%ScriptPath%%WatchdogLog%.txt"
cmd /c START /max "" "%ScriptPath%LaunchApp.bat"
SET /A "LaunchCount=LaunchCount+1"
ECHO %DATE% %TIME% - App was not running, launched. LaunchCount is now %LaunchCount%. Waiting %LaunchTimeout% secs to scan again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %LaunchTimeout% /NOBREAK>NUL
GOTO DetectApp





:AppHang

REM *** If we are here, the hearbeat was more than x seconds old, as specified in HeartbeatTimeout ***

ECHO %DATE% %TIME% - Heartbeat is older than %HeartbeatTimeout% secs (file time is %FileStamp%, which is older than %TimeLimit%), terminating app... >>"%ScriptPath%%WatchdogLog%.txt"
taskkill /IM yanga_app.exe /f>NUL
IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - App terminated, waiting %TerminateTimeout% secs before launching again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %TerminateTimeout% /NOBREAK>NUL
IF %LaunchCount% GEQ %LaunchLimit% GOTO RebootNow
IF %DebugFlag% GTR 0 ECHO %DATE% %TIME% - Launch limit not reached (%LaunchCount%/%LaunchLimit%), Launching app... >>"%ScriptPath%%WatchdogLog%.txt"
cmd /c START /max "" "%ScriptPath%LaunchApp.bat"
SET /A "LaunchCount=LaunchCount+1"
ECHO %DATE% %TIME% - Heartbeat was too old, terminated app and launched again. LaunchCount is now %LaunchCount%. Waiting %LaunchTimeout% secs to scan again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %LaunchTimeout% /NOBREAK>NUL
GOTO DetectApp





:FileTimeNotRead

REM *** If we are here, the output of reading the heartbeat time is not correct. We will skip heartbeat detection for the remainder of the session ***

SET /A "ScanHeartbeat=0"
ECHO %DATE% %TIME% - Heartbeat time readout failed, skipping heartbeat detection for the remainder of the session. ScanHeartbeat is now %ScanHeartbeat%. >>"%ScriptPath%%WatchdogLog%.txt"
IF %DebugFlag% GTR 1 ECHO %DATE% %TIME% - Waiting %IdleTimeout% secs to scan again. >>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %IdleTimeout% /NOBREAK>NUL
GOTO DetectApp





:SkipScan

REM *** If we are here ScanHeartbeat is set to 0 and we just skipped the scan procedure. Waiting to scan again. ***

ECHO %DATE% %TIME% - Heartbeat scan skipped. Waiting %IdleTimeout% secs to scan again.>>"%ScriptPath%%WatchdogLog%.txt"
TIMEOUT /T %IdleTimeout% /NOBREAK>NUL
GOTO DetectApp





:RebootNow

REM *** If we are here, the launch limit as specified in LaunchLimit reached, so we can't launch again and must reboot ***

ECHO %DATE% %TIME% - Launch limit (%LaunchLimit%) reached, rebooting. >>"%ScriptPath%%WatchdogLog%.txt"
SHUTDOWN -r -f -t 0